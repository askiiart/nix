{config, pkgs, ... }:

{
  home.sessionVariables = {
    DOTNET_ROOT = "${pkgs.dotnet-runtime_7}";
  };

  programs.git = {
    enable = true;
    lfs.enable = true;
    userName  = "askiiart";
    userEmail = "dev@askiiart.net";
    extraConfig = {
      commit.gpgsign = true;
      user.signingkey = "02EFA1CE3C3E4AAD7A863AB8ED24985CA884CD61";
      push = { autoSetupRemote = true; };
      credential = {
        credentialStore = "gpg";        
        helper = "${pkgs.git-credential-manager}/bin/git-credential-manager";
      };
    };
  };

  gtk = {
    enable = true;
    theme = {
      name = "Catppuccin-Mocha-Compact-Mauve-Dark";
      package = pkgs.catppuccin-gtk.override {
        accents = [ "mauve" ];
        size = "compact";
        tweaks = [ "normal" ];
        variant = "mocha";
      };
    };
  };

  # Now symlink the `~/.config/gtk-4.0/` folder declaratively:
  xdg.configFile = {
    "gtk-4.0/assets".source = "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/assets";
    "gtk-4.0/gtk.css".source = "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/gtk.css";
    "gtk-4.0/gtk-dark.css".source = "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/gtk-dark.css";
  };
}
