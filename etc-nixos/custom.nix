{ config, lib, pkgs, ... }:

let
  swayConfig = pkgs.writeText "greetd-sway-config" ''
    # `-l` activates layer-shell mode. Notice that `swaymsg exit` will run after gtkgreet.
    exec "${pkgs.greetd.gtkgreet}/bin/gtkgreet -l; swaymsg exit"
    bindsym Mod4+shift+e exec swaynag \
      -t warning \
      -m 'What do you want to do?' \
      -b 'Poweroff' 'systemctl poweroff' \
      -b 'Reboot' 'systemctl reboot'
  '';

  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  # packages and other environment stuff
  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = [
    pkgs.polkit_gnome
    pkgs.sway
    pkgs.imagemagick
    pkgs.greetd.gtkgreet
    pkgs.fish
    pkgs.qemu
    pkgs.home-manager
    pkgs.mesa
    pkgs.meslo-lgs-nf
    pkgs.libvirt
    #pkgs.flatpak
    #pkgs.xdg-desktop-portal-gtk
    pkgs.iotop
    pkgs.gparted
    pkgs.dbus
    pkgs.xdg-utils
    pkgs.spamassassin
    #pkgs.docker
    pkgs.linuxConsoleTools
    pkgs.podman
  ];

  # enable podman
  virtualisation.podman = {
    enable = true;
  };

  # enable spamd
  services.spamassassin.enable = true;

  # xdg-desktop-portal works by exposing a series of D-Bus interfaces
  # known as portals under a well-known name
  # (org.freedesktop.portal.Desktop) and object path
  # (/org/freedesktop/portal/desktop).
  # The portal interfaces include APIs for file access, opening URIs,
  # printing and others.
  services.dbus.enable = true;
  xdg.portal = {
    enable = true;
    wlr.enable = true;
    # gtk portal needed to make gtk apps happy
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    configPackages = [ pkgs.xdg-desktop-portal-gtk ];
  };

  # Enable the gnome-keyring secrets vault. 
  # Will be exposed through DBus to programs willing to store secrets.
  services.gnome.gnome-keyring.enable = true;

  # enable flatpak
  # disabled because the XDG portal adds like a 30 second delay to opening most programs, and I don't actually use flatpaks so whatever
  #services.flatpak.enable = true;
  #xdg.portal.enable = true;
  #xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  #xdg.portal.config = [ "xdg-deskop-portal-gtk" ];
  #xdg.portal.config.common.default = "*";

  # enable virt
  virtualisation.libvirtd.enable = true;
  programs.virt-manager.enable = true;

  # enable opengl
  hardware.opengl.enable = true;

  # fix swaylock never unlocking
  security.pam.services.swaylock = {
    text = ''
      auth include login
    '';
  };

  # Set up shells
  programs.fish.enable = true;

  # polkit (auth agent) config
  security.polkit.enable = true;

  # Set up greetd (gtkgreet)
  services.greetd = {
    enable = true;
    settings = {
      default_session = {
        command = "${pkgs.sway}/bin/sway --config ${swayConfig}";
      };
    };
  };
  environment.etc."greetd/environments".text = ''
    sway
    fish
    bash
  '';

  # gnupg agent settings
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryFlavor = "gtk2";
    settings = {
      default-cache-ttl = 60;
      max-cache-ttl = 120;
    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.askiiart = {
    isNormalUser = true;
    extraGroups = [ "wheel" "libvirtd" "audio" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.fish;
  };

  # for qemu
  boot.extraModprobeConfig = "options kvm_intel nested=1";

  # fonts
    fonts = {
    packages = with pkgs; [
      atkinson-hyperlegible
      (nerdfonts.override { fonts = [ "FiraCode" ]; })
      noto-fonts
      jetbrains-mono
    ];

    fontDir.enable = true;
    
    enableDefaultPackages = true;
    
    fontconfig.defaultFonts = {
      sansSerif = [ "Atkinson Hyperlegible" ];
      serif = [ "Atkinson Hyperlegible" ];
      monospace = [ "FiraCode Nerd Font" ];
    };
  };

  # for external drives showing in file managers
  services.devmon.enable = true;
  services.gvfs.enable = true;
  services.udisks2.enable = true;

  # polkit config
  # allow unprivileged users to reboot and poweroff
  security.polkit.extraConfig = ''
    polkit.addRule(function(action, subject) {
      if (
        subject.isInGroup("users")
          && (
            action.id == "org.freedesktop.login1.reboot" ||
            action.id == "org.freedesktop.login1.reboot-multiple-sessions" ||
            action.id == "org.freedesktop.login1.power-off" ||
            action.id == "org.freedesktop.login1.power-off-multiple-sessions"
          )
        )
      {
        return polkit.Result.YES;
      }
    })
  '';

  # polkit authentication agent (gnome_polkit)
  systemd = {
  user.services.polkit-gnome-authentication-agent-1 = {
    description = "polkit-gnome-authentication-agent-1";
    wantedBy = [ "graphical-session.target" ];
    wants = [ "graphical-session.target" ];
    after = [ "graphical-session.target" ];
    serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };
  };

  # enable bluetooth and fix various audio issues
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;
  services.blueman.enable = true;
  # enable more codecs
  hardware.bluetooth.settings = {
    General = {
      Enable = "Source,Sink,Media";
      Disable = "Socket";
      MultiProfile= "multiple";
    };
  };
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
  };
  # use bluetooth headset buttons
  systemd.user.services.mpris-proxy = {
      description = "Mpris proxy";
      after = [ "network.target" "sound.target" ];
      wantedBy = [ "default.target" ];
      serviceConfig.ExecStart = "${pkgs.bluez}/bin/mpris-proxy";
  };
  nixpkgs.config.pulseaudio = true;
  hardware.pulseaudio.extraConfig = "load-module module-combine-sink module-dbus-protocol";

  # fix Steam glXChooseVisual failed
  hardware.opengl.driSupport32Bit = true;

  # "fix" broken gamepad
  #services.cron = {
  #  enable = true;
  #  systemCronJobs = [
  #    "* * * * * root evdev-joystick --e /dev/input/by-id/usb-ShanWan_Xbox360_For_Windows_10F36D6-event-joystick --minimum -32767 --maximum 0 --a 3; evdev-joystick --e /dev/input/by-id/usb-ShanWan_Xbox360_For_Windows_10F36D6-event-joystick --minimum 0 --maximum 32767 --a 4"
  #  ];
  #};

  systemd.services.fix-gamepad = {
    enable = true;
    unitConfig.Type = "simple";
    description = "fix the gamepad by adding offset to the right stick";
    serviceConfig.ExecStart = "/run/current-system/sw/bin/bash -c 'while true; do /run/current-system/sw/bin/evdev-joystick --e /dev/input/by-id/usb-ShanWan_Xbox360_For_Windows_10F36D6-event-joystick --minimum -32767 --maximum 0 --a 3; /run/current-system/sw/bin/evdev-joystick --e /dev/input/by-id/usb-ShanWan_Xbox360_For_Windows_10F36D6-event-joystick --minimum 0 --maximum 32767 --a 4; sleep 5; done'";
    wantedBy = [ "multi-user.target" ];
  };

  # enable the nix command
  nix.settings.experimental-features = [ "nix-command" ];

  # optimize the nix store
  #nix.optimise.automatic = true;
  nix.settings.auto-optimise-store = true;

  nix.gc = {
    automatic = true;
    dates = "daily";
    options = "--delete-older-than 2d";
  };
}
