#!/usr/bin/env bash
GIT_NAME="askiiart"
GIT_EMAIL="dev@askiiart.net"
KEY_FINGERPRINT="02EFA1CE3C3E4AAD7A863AB8ED24985CA884CD61"

if [ $(whoami) == "root" ]; then
    echo "Run as a normal user, not root"
    exit 1
fi

command_exists() { type "$1" &>/dev/null; }

if ! (command_exists "git" && command_exists "pass" && command_exists "dotnet" && command_exists "git-credential-manager"); then
    echo "Install git, pass, dotnet runtime 7, and git-credential-manager first, then run this script again."
    echo "pkgs.git, pkgs.pass, pkgs.dotnet-runtime_7, and pkgs.git-credential.manager"
    exit 1
fi

read -p "Insert the Yubikey, and press enter. Then enter fetch, quit, trust, 5, quit."
gpg --edit-card
gpg --edit-key $KEY_FINGERPRINT


# Set up commit verification and gpg credential store
git config --global user.name "${GIT_NAME}"
git config --global user.email "${GIT_EMAIL}"
git-credential-manager configure
git config --global commit.gpgsign true
git config --global credential.credentialStore gpg

git config --global user.signingkey ${KEY_FINGERPRINT}
pass init ${KEY_FINGERPRINT}

# From https://superuser.com/a/954639
# Archived at https://web.archive.org/web/20230606153856/https://superuser.com/a/954639
echo Fixing .gnupg/ permissions
# Set ownership to your own user and primary group
chown -R "$USER:$(id -gn)" ~/.gnupg
# Set permissions to read, write, execute for only yourself, no others
chmod 700 ~/.gnupg
# Set permissions to read, write for only yourself, no others
chmod 600 ~/.gnupg/*
# Fixes dirmngr stuff
sudo chmod 700 $(ls -d $HOME/.gnupg/*/)
