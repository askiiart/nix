#!/usr/bin/env bash
SCRIPT_LOCATION=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

rm -rf $SCRIPT_LOCATION/etc-nixos
cp -r /etc/nixos/ $SCRIPT_LOCATION/etc-nixos

rm -rf $SCRIPT_LOCATION/home-manager
cp -r $HOME/.config/home-manager/ $SCRIPT_LOCATION/home-manager
