#!/usr/bin/env bash
SCRIPT_LOCATION=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

sudo cp -r $SCRIPT_LOCATION/etc-nixos/* /etc/nixos/

rm -rf $HOME/.config/home-manager/
mkdir -p $HOME/.config/home-manager/
cp -r $SCRIPT_LOCATION/home-manager/* $HOME/.config/home-manager/
